defmodule RadioAt.Repo.Migrations.AddStationsAndStreams do
  use Ecto.Migration

  def change do
    create table("stations") do
      add :name, :text
      add :notes, :text
      add :homepage, :text

      timestamps()
    end

    create table("streams") do
      add :station_id, references("stations")
      add :url, :text, null: false
      add :name, :text
      add :bitrate, :integer
      add :format, :string

      timestamps()
    end
  end
end
