import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :radio_at, RadioAt.Repo,
  username: "postgres",
  password: "postgres",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  database: "radio_test#{System.get_env("CI") && System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :radio_at, RadioAtWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "80Bv6yVuSdkaSFkwV5B3oOvdZN+nvzxCXqnj8YQ456MGTVqBfhHBZJ661N18jTfs",
  server: false

# In test we don't send emails.
config :radio_at, RadioAt.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :radio_at, Oban, testing: :inline

config :radio_at, httpoison: HTTPoisonMock
