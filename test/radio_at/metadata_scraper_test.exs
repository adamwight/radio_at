defmodule RadioAt.MetadataScraperTest do
  use ExUnit.Case
  alias RadioAt.MetadataScraper
  import Mox

  setup :verify_on_exit!

  test "m3u playlist" do
    HTTPoisonMock
    # from https://stream.kalx.berkeley.edu:8443/kalx-128.mp3.m3u
    |> expect(:get!, fn "https://stream.test/example.mp3.m3u", _, _ ->
      %{
        status_code: 200,
        headers: [
          {"Server", "Icecast 2.4.4"},
          {"Content-Type", "audio/mpegurl"}
        ],
        body: "https://stream.test/example.mp3"
      }
    end)
    |> expect(:get!, fn "https://stream.test/example.mp3", _, _ ->
      %{
        status_code: 200,
        headers: [{"Content-Type", "audio/mpeg"}],
        body: ""
      }
    end)

    actual =
      MetadataScraper.scrape_url("https://stream.test/example.mp3.m3u")

    assert_streams(
      [
        %RadioAt.Stream{
          format: "audio/mpeg",
          url: "https://stream.test/example.mp3"
        }
      ],
      actual
    )
  end

  test ".pls with two streams" do
    HTTPoisonMock
    # from https://wfmu.org/wfmu.pls
    |> expect(:get!, fn "https://stream.test/example.pls", _, _ ->
      %{
        status_code: 200,
        headers: [{"Content-Type", "audio/x-scpls"}],
        body: """
          [playlist]
          numberofentries=4
          Title1=ABCD - Example Radio
          File1=http://stream0.test/example-high.aac
          Title2=ABCD - Example Radio - Backup
          File2=http://stream2.test/example-128k
        """
      }
    end)
    |> expect(:get!, fn "http://stream0.test/example-high.aac", _, _ ->
      %{
        status_code: 200,
        headers: [{"Content-Type", "audio/aacp"}],
        body: ""
      }
    end)
    |> expect(:get!, fn "http://stream2.test/example-128k", _, _ ->
      %{
        status_code: 200,
        headers: [{"Content-Type", "audio/mpeg"}],
        body: ""
      }
    end)

    actual =
      MetadataScraper.scrape_url("https://stream.test/example.pls")

    assert_streams(
      [
        %RadioAt.Stream{
          format: "audio/mpeg",
          url: "http://stream2.test/example-128k"
        },
        %RadioAt.Stream{
          format: "audio/aacp",
          url: "http://stream0.test/example-high.aac"
        }
      ],
      actual
    )
  end

  test ".xspf" do
    HTTPoisonMock
    # from https://punkirratia.net:8443/punk.xspf
    |> expect(:get!, fn "https://stream.test/example.xspf", _, _ ->
      %{
        status_code: 200,
        headers: [
          {"Content-Type", "application/xspf+xml; charset=UTF-8"},
          {"X-Robots-Tag",
           "noindex, nofollow, nosnippet, noarchive, noodp, notranslate, noimageindex"}
        ],
        body: ~s(<?xml version="1.0" encoding="UTF-8"?>
          <playlist xmlns="http://xspf.org/ns/0/" version="1">
            <title/>
            <creator/>
            <trackList>
              <track>
                <location>http://stream.test:8000/example</location>
                <title>Song Name</title>
                <annotation>Stream Title: punk
          Stream Description: Long description
          Content Type:audio/mpeg
          Bitrate: 192
          Current Listeners: 6
          Peak Listeners: 34
          Stream Genre: punk anarcho-punk hardcore</annotation>
                <info>https://web.test</info>
              </track>
            </trackList>
          </playlist>
        )
      }
    end)
    |> expect(:get!, fn "http://stream.test:8000/example", _, _ ->
      %{
        status_code: 200,
        headers: [{"Content-Type", "audio/mpeg"}],
        body: ""
      }
    end)

    actual =
      MetadataScraper.scrape_url("https://stream.test/example.xspf")

    assert_streams(
      [
        %RadioAt.Stream{
          format: "audio/mpeg",
          url: "http://stream.test:8000/example"
        }
      ],
      actual
    )
  end

  test "plain stream with homepage and lots of metadata" do
    HTTPoisonMock
    |> expect(:get!, fn "https://stream.test/example.mp3", _, _ ->
      %{
        status_code: 200,
        headers: [
          {"Server", "Icecast 2.4.4"},
          {"Content-Type", "audio/mpeg"},
          {"icy-br", "128"},
          # Can also be like ice-audio-info: channels=2;samplerate=44100;bitrate=192
          {"ice-audio-info", "ice-samplerate=44100;ice-bitrate=128;ice-channels=2"},
          {"icy-description", "Example long description"},
          {"icy-genre", "Example_Genre another-genre"},
          {"icy-name", "Example Radio"},
          {"icy-pub", "1"},
          {"icy-url", "http://web.test"},
          {"icy-index-metadata", "1"},
          {"icy-logo", "https://stream.test:8443/punkirratia.jpg"},
          {"icy-country-code", "es"},
          {"icy-language-codes", "eu,en,es"},
          # {"icy-main-stream-url", "https://stream.test:8443/punk"},
          {"icy-geo-lat-long", "43.257772,-2.898934"}
        ],
        body: ""
      }
    end)
    |> expect(:get!, fn "http://web.test", _, _ ->
      %{status_code: 200, headers: [{"Content-Type", "text/html"}], body: ""}
    end)

    actual =
      MetadataScraper.scrape_url("https://stream.test/example.mp3")

    assert_streams(
      [
        %RadioAt.Stream{
          name: "Example Radio",
          bitrate: 128,
          format: "audio/mpeg",
          url: "https://stream.test/example.mp3"
        }
      ],
      actual
    )
  end

  defp assert_streams(expected, actual) do
    assert expected == Enum.map(actual.streams, &Ecto.Changeset.apply_changes/1)
  end
end
