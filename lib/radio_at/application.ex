defmodule RadioAt.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      RadioAtWeb.Telemetry,
      # Start the Ecto repository
      RadioAt.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: RadioAt.PubSub},
      # Start Finch
      {Finch, name: RadioAt.Finch},
      # Start the Endpoint (http/https)
      RadioAtWeb.Endpoint,
      # Start a worker by calling: RadioAt.Worker.start_link(arg)
      # {RadioAt.Worker, arg}
      {Oban, Application.fetch_env!(:radio_at, Oban)}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RadioAt.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    RadioAtWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
