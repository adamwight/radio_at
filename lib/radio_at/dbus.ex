defmodule RadioAt.Dbus do
  @dbus_enabled Application.compile_env(:radio_at, :dbus)

  defmodule Message do
    require Record
    Record.defrecord(:dbus_message, header: :undefined, body: :undefined)
  end

  defmodule MediaPlayer2 do
    def available?() do
      if RadioAt.Dbus.enabled?() do
        RadioAt.Dbus.list_names()
        |> Enum.any?(&String.starts_with?(&1, "org.mpris.MediaPlayer2"))
      end
    end

    # FIXME: query each detected player
    def now_playing() do
      props =
        RadioAt.Dbus.get_all_properties(
          "org.mpris.MediaPlayer2.rhythmbox",
          "/org/mpris/MediaPlayer2",
          "org.mpris.MediaPlayer2.Player"
        )

      case props["PlaybackStatus"] do
        "Playing" -> props["Metadata"]
        _ -> nil
      end
    end

    def now_playing_title() do
      meta = now_playing()
      meta["rhythmbox:streamTitle"] <> ": " <> meta["xesam:title"]
    end

    def now_playing_url() do
      meta = now_playing()
      meta["xesam:url"]
    end
  end

  def enabled?(), do: @dbus_enabled

  # TODO:
  # RadioAt.Dbus.send("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus.ListActivatableNames") |> IO.puts()
  #
  # xesam:url
  # xesam:title vs rhythmbox:streamTitle
  # xesam:useCount
  # PlaybackStatus
  #
  # TODO: listen to org.freedesktop.DBus.Properties.PropertiesChanged signal

  # TODO: message encoding is broken.  Leave a backdoor for hardcoded schema?
  def send(dest, object_name, interface, action) do
    require RadioAt.Dbus.Message

    {:ok, pid} = :dbus_bus_connection.connect(:session)

    msg =
      :dbus_message.call(
        to_charlist(dest),
        to_charlist(object_name),
        to_charlist(interface),
        to_charlist(action)
      )

    # TODO: Setting the body is hard.

    {:ok, resp} = :dbus_bus_connection.call(pid, msg)
    RadioAt.Dbus.Message.dbus_message(resp)
  end

  def list_names() do
    msg =
      RadioAt.Dbus.send(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        "org.freedesktop.DBus",
        "ListNames"
      )

    msg[:body]
  end

  def list_activatable_names() do
    msg =
      RadioAt.Dbus.send(
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        "org.freedesktop.DBus",
        "ListActivatableNames"
      )

    msg[:body]
  end

  def get_all_properties(dest, object_name, prop_interface) do
    {:ok, pid} = :dbus_bus_connection.connect(:session)
    {:ok, bus} = GenServer.start_link(:dbus_bus, [pid, self()], [])

    {:ok, service} = :dbus_bus.get_service(bus, dest)
    {:ok, remote_object} = :dbus_remote_service.get_object(service, object_name)
    {:ok, iface} = :dbus_proxy.interface(remote_object, "org.freedesktop.DBus.Properties")

    {:ok, resp} = :dbus_proxy.call(iface, "GetAll", [prop_interface])

    :ok = :dbus_bus_connection.close(bus)

    resp
  end
end
