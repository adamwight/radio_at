defmodule RadioAt.Station do
  use Ecto.Schema
  import Ecto.Changeset
  alias RadioAt.Stream

  schema "stations" do
    field :name, :string
    field :notes, :string
    field :homepage, :string

    has_many :streams, Stream

    timestamps()
  end

  def changeset(station, params \\ %{}) do
    station
    |> cast(params, ~w(name notes homepage)a)
    |> cast_assoc(:streams)
  end
end
