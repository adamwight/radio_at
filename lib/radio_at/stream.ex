defmodule RadioAt.Stream do
  use Ecto.Schema
  import Ecto.Changeset
  alias RadioAt.Station

  schema "streams" do
    belongs_to :station, Station

    field :url, :string
    field :name, :string
    field :bitrate, :integer
    field :format, :string

    timestamps()
  end

  def changeset(stream, params \\ %{}) do
    stream
    |> cast(params, ~w(url name bitrate format)a)
    |> validate_required(~w(url)a)
    |> unique_constraint(:url)
  end
end
