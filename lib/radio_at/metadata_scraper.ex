defmodule RadioAt.MetadataScraper do
  use Gettext, backend: RadioAtWeb.Gettext
  alias RadioAt.Station
  alias RadioAt.Stream
  require Logger

  defmodule XmlText do
    require Record

    Record.defrecord(:xmlText,
      parents: :undefined,
      pos: :undefined,
      language: :undefined,
      value: :undefined,
      type: :undefined
    )
  end

  @type results :: %{
          streams: list(Ecto.Changeset.t(%Stream{})),
          seen_urls: MapSet.t(),
          station: Ecto.Changeset.t(%Station{})
        }

  @spec scrape_url(binary) :: results()
  def scrape_url(url),
    do:
      do_scrape_url(
        %{seen_urls: MapSet.new(), streams: [], station: Station.changeset(%Station{})},
        url
      )

  @spec do_scrape_url(results(), binary()) :: results()
  # FIXME: should never see a nil
  def do_scrape_url(results, nil), do: results

  def do_scrape_url(results, url) do
    if MapSet.member?(results.seen_urls, url) do
      results
    else
      scrape_response(results, url, fetch(url))
    end
  end

  @spec scrape_response(results(), binary(), HTTPoison.Response.t()) ::
          Ecto.Changeset.t(%Station{})
  def scrape_response(results, url, response) do
    %{status_code: 200, headers: headers, body: body} = response

    headers = normalize_headers(headers)

    %{results | seen_urls: MapSet.put(results.seen_urls, url)}
    |> scrape_by_content_type(url, headers, body)

    # |> icecast_status()
  end

  defp scrape_by_content_type(results, url, headers, body)

  defp scrape_by_content_type(results, _, %{"content-type" => "application/xspf+xml"}, body),
    do: do_scrape_url(results, parse_xspf_for_url(body))

  defp scrape_by_content_type(results, _, %{"content-type" => "audio/mpegurl"}, body),
    do: do_scrape_url(results, String.trim(body))

  defp scrape_by_content_type(results, _, %{"content-type" => "audio/x-mpegurl"}, body),
    do: do_scrape_url(results, String.trim(body))

  defp scrape_by_content_type(results, _, %{"content-type" => "audio/x-scpls"}, body),
    do: parse_pls(results, body)

  defp scrape_by_content_type(results, url, %{"content-type" => "audio/aacp"} = headers, _) do
    results
    |> add_stream(scrape_stream_headers(headers) |> Map.put(:url, url))
    |> crawl_header_links(headers)
  end

  defp scrape_by_content_type(results, url, %{"content-type" => "audio/mpeg"} = headers, _) do
    results
    |> add_stream(scrape_stream_headers(headers) |> Map.put(:url, url))
    |> crawl_header_links(headers)
  end

  defp scrape_by_content_type(results, _url, %{"content-type" => content_type}, _) do
    %{
      results
      | station:
          results.station
          |> Station.changeset(%{name: gettext("Unknown media type %{type}", type: content_type)})
    }
  end

  @http_client Application.compile_env(:radio_at, :httpoison)

  defp fetch(url) do
    # FIXME: upstream or SSL cert bug seen on kalx
    # response = HTTPoison.get!(url)
    # response = HTTPoison.get!(url, ssl: [versions: [:"tlsv1.2"]])
    @http_client.get!(url, [],
      # TODO: async request, stop or buffer output when streaming, complete
      # when metadata...
      follow_redirect: true,
      max_body_length: 1024 * 16,
      ssl: [versions: [:"tlsv1.2"]]
    )

    # response = HTTPoison.get!(url, [], hackney: [:insecure])
  end

  def icecast_status(_results) do
    # TODO: check that this was icecast >= 2.4.0

    # FIXME: homepage links to web page and not https://stream.kalx.berkeley.edu:8443/status-json.xsl
    # See also:
    # https://stream.kalx.berkeley.edu:8443/server_version.xsl
    # https://stream.kalx.berkeley.edu:8443/status.xsl

    # FIXME: run on each URL

    # response = fetch(results.homepage <> "/status-json.xsl")
  end

  def scrape_stream_headers(headers) do
    %{
      format: headers["content-type"],
      bitrate: headers["icy-br"],
      name: headers["icy-name"]
    }
  end

  def crawl_header_links(results, headers) do
    results
    # TODO: also use information about which headers it came from
    |> do_scrape_url(headers["icy-url"])
    |> do_scrape_url(headers["icy-main-stream-url"])
  end

  defp add_stream(results, params) do
    stream =
      %Stream{}
      |> Stream.changeset(params)

    results
    |> Map.update!(:streams, fn streams ->
      [stream | streams]
    end)
  end

  def normalize_headers(headers) do
    headers
    |> Map.new(fn {name, value} ->
      {String.downcase(name), value}
    end)
    |> Map.filter(fn {key, _} ->
      key in ~w(content-type server) or String.starts_with?(key, "icy-")
    end)
    |> Map.update!("content-type", fn value -> String.replace(value, ~r/;.*$/, "") end)
  end

  def get_headers(headers, name_regex) do
    headers
    |> Enum.filter(fn {key, _} ->
      String.match?(key, name_regex)
    end)
  end

  def get_header_value(headers, name) do
    headers
    |> Enum.find_value(fn
      {^name, value} -> value
      _ -> nil
    end)
  end

  def parse_pls(results, body) do
    pls =
      body
      |> String.split(~r/[\r\n]+/)
      |> Enum.flat_map(fn line ->
        line
        |> String.split("=", parts: 2, trim: true)
        |> case do
          [key, value] -> [{String.downcase(String.trim(key)), String.trim(value)}]
          _ -> []
        end
      end)
      |> Enum.into(%{})

    1..min(String.to_integer(pls["numberofentries"]), 10)
    |> Enum.reduce(results, fn index, res ->
      # TODO: keep title#{index}
      res
      |> do_scrape_url(pls["file#{index}"])
    end)
  end

  def parse_xspf_for_url(xml) do
    require RadioAt.MetadataScraper.XmlText

    {xmerl, _} =
      xml
      |> :erlang.binary_to_list()
      |> :xmerl_scan.string()

    [urlElement] = :xmerl_xpath.string(~c"//track/location/text()", xmerl)
    urlText = XmlText.xmlText(urlElement)

    to_string(urlText[:value])
  end
end
