defmodule RadioAt.ScheduledRecording do
  use Oban.Worker

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"station" => station, "type" => type}}) do
    # TODO: uniqueness
    # TODO:
    # - Record metadata about the recording so it can be found by the UI
    # - Tee stream to a file
    # - Analyze metadata tags in the stream, build a playlist
    # - Stop at a finite time
    # - Detect and log downsampling adaptation

    # model = RadioAt.Repo.get(RadioAt.Station, station_id)

    case type do
      _ ->
        # TODO: Supervise and link, keep this worker runnin
        RadioAt.FileRecording.start_link(station)
    end

    :ok
  end
end

defmodule RadioAt.FileRecording do
  use Membrane.Pipeline

  @impl true
  def handle_init(_ctx, url) do
    spec =
      child(:source, %Membrane.Hackney.Source{location: url})
      |> child(:file, %Membrane.File.Sink{location: "/dev/null"})

    # |> child(:file, %Membrane.File.Sink{location: "/tmp/radioat.out"})

    {[spec: spec], %{}}
  end
end
