defmodule RadioAt.Repo do
  use Ecto.Repo,
    otp_app: :radio_at,
    adapter: Ecto.Adapters.Postgres
end
