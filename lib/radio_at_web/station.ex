defmodule RadioAtWeb.Station do
  use RadioAtWeb, :live_view
  alias RadioAt.MetadataScraper

  import RadioAtWeb.StationComponents

  def mount(%{"site" => station}, _session, socket) do
    {:ok,
     socket
     |> assign(:station, station)
     |> assign(
       :station_metadata,
       station
       |> String.trim()
       |> MetadataScraper.scrape_url()
     )}
  end

  def render(assigns) do
    ~H"""
    <.station_or_menu station={@station_metadata} />
    """
  end

  def handle_event("record_now", _params, socket) do
    %{
      "station" => socket.assigns.station,
      "type" => socket.assigns.station_metadata.content_type
    }
    |> RadioAt.ScheduledRecording.new(schedule_in: 5)
    |> Oban.insert!()

    {:noreply,
     socket
     |> put_flash(:info, gettext("Beginning recording"))
     |> push_navigate(to: ~p"/status")}
  end
end
