defmodule RadioAtWeb.PageHTML do
  use RadioAtWeb, :html

  embed_templates "page_html/*"
end
