defmodule RadioAtWeb.Status do
  use RadioAtWeb, :live_view

  import RadioAtWeb.StationComponents

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(
       :jobs,
       scan_jobs()
     )}
  end

  def render(assigns) do
    ~H"""
    <div class="flex flex-col">
      <div class="w-full">
        <%= for job <- @jobs do %>
          <div class="border p-2">
            <.station_label name={job.args["station"]} station={job.args["station"]} />
            <p><%= gettext("Status: %{state}", state: job.state) %></p>
            <p><%= gettext("Started at %{time}", time: job.attempted_at) %></p>
            <p><%= gettext("Ended at %{time}", time: job.completed_at) %></p>
          </div>
        <% end %>
      </div>
    </div>
    """
  end

  def scan_jobs() do
    Oban
    |> Oban.config()
    |> Oban.Repo.all(Oban.Job)
  end
end
