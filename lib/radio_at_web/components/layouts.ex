defmodule RadioAtWeb.Layouts do
  use RadioAtWeb, :html

  embed_templates "layouts/*"
end
