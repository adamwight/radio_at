defmodule RadioAtWeb.StationComponents do
  use Phoenix.Component
  import RadioAtWeb.CoreComponents
  use Gettext, backend: RadioAtWeb.Gettext

  use Phoenix.VerifiedRoutes,
    endpoint: RadioAtWeb.Endpoint,
    router: RadioAtWeb.Router,
    statics: RadioAtWeb.static_paths()

  def station_or_menu(assigns) do
    ~H"""
    <div class="flex">
      <div class="w-full sm:w-auto border">
        <.station_menu menu={@station.streams} />
      </div>
    </div>
    """
  end

  def station_label(assigns) do
    ~H"""
    <.link navigate={~p"/station?site=#{@station}"}>
      <h1 class="text-lg"><%= @name %></h1>
    </.link>
    """
  end

  def station(assigns) do
    # FIXME: structs from db rather than changesets
    assigns =
      assigns
      |> Map.update!(:station, fn changeset ->
        Ecto.Changeset.apply_action!(changeset, :insert)
      end)

    ~H"""
    <.site station={@station} />
    <.server station={@station} />
    <.bitrate station={@station} />
    <.station_actions station={@station} />
    """
  end

  def station_actions(assigns) do
    ~H"""
    <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-4 sm:grid-cols-3">
      <button
        phx-click="record_now"
        type="button"
        class="flex-none p-3 bg-red-400"
        aria-label={gettext("record now")}
      >
        <.icon name="hero-play-pause" class="h-5 w-5" />
        <%= gettext("Begin recording now") %>
      </button>
      <.link
        navigate={~p"/schedule?station=#{@station.url}"}
        class="flex-none p-3 bg-yellow-400"
        aria-label={gettext("schedule recording")}
      >
        <.icon name="hero-clock" class="h-5 w-5" />
        <%= gettext("Set up recording schedule") %>
      </.link>
      <button
        phx-click="remember_station"
        type="button"
        class="flex-none p-3 bg-green-400"
        aria-label={gettext("Remember station")}
      >
        <.icon name="hero-arrow-down-tray" class="h-5 w-5" />
        <%= gettext("Remember station") %>
      </button>
    </div>
    """
  end

  def station_menu(assigns) do
    ~H"""
    <div class="flex flex-col gap-2">
      <%= for stream <- @menu do %>
        <.station station={stream} />
      <% end %>
    </div>
    """
  end

  def server(assigns) do
    ~H"""
    <%= if @station.url do %>
      <div class="flex flex-row items-center">
        <h2 class="text-lg p-2"><%= gettext("Server") %></h2>
        <p class="p-2"><%= @station.url %></p>
      </div>
    <% end %>
    """
  end

  def bitrate(assigns) do
    ~H"""
    <%= if @station.bitrate do %>
      <div class="flex flex-row items-center">
        <h2 class="text-lg p-2"><%= gettext("Bitrate") %></h2>
        <p class="p-2"><%= gettext("%{bitrate} kbps", bitrate: @station.bitrate) %></p>
      </div>
    <% end %>
    """
  end

  def site(assigns) do
    ~H"""
    <%= if @station.name do %>
      <div class="flex flex-row items-center">
        <h1 class="text-xl p-2"><%= @station.name %></h1>
        <!--
        < %= if @station.homepage do %>
          <p class="p-2"><a href={@station.homepage}>< %= @station.homepage %></a></p>
        < % end %>
        -->
      </div>
    <% else %>
      <h1 class="text-xl p-2"><%= gettext("Untitled station") %></h1>
    <% end %>
    """
  end
end
