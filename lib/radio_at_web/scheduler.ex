defmodule RadioAtWeb.Scheduler do
  use RadioAtWeb, :live_view

  def mount(%{"station" => station}, _session, socket) do
    latest_hour =
      %Time{
        Time.utc_now()
        | minute: 0,
          second: 0,
          microsecond: {0, 0}
      }

    next_hour =
      latest_hour
      |> Time.add(1, :hour)

    {
      :ok,
      socket
      |> assign(:locales, Tzdata.zone_list())
      |> assign(:station, station)
      |> assign(
        :form,
        to_form(%{
          # TODO: Default to the browser's
          # Intl.DateTimeFormat().resolvedOptions().timeZone ?
          "timezone" => "Etc/UTC",
          "start_time" => latest_hour |> Time.to_string(),
          "end_time" => next_hour |> Time.to_string()
        })
      )
    }
  end

  def render(assigns) do
    ~H"""
    <pre><%= @station %></pre>
    <.form for={@form} phx-submit="schedule">
      <.input list="zones" name="timezone" label={gettext("Time zone")} field={@form[:timezone]} />
      <datalist id="zones">
        <%= for locale <- @locales do %>
          <option value={locale}>
            <%= locale %>
          </option>
        <% end %>
      </datalist>

      <.input
        name="start_time"
        type="time"
        step="1800"
        label={gettext("Start time")}
        field={@form[:start_time]}
      />
      <.input
        name="end_time"
        type="time"
        step="1800"
        label={gettext("End time")}
        field={@form[:end_time]}
      /> Repeat? no / weekly and days of week
    </.form>
    """
  end
end
