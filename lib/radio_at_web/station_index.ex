defmodule RadioAtWeb.StationIndex do
  use RadioAtWeb, :live_view

  # @example_site "https://stream.kalx.berkeley.edu:8443/kalx-128.mp3.m3u"
  @example_site "https://wfmu.org/wfmu.pls"
  # @example_site "http://stream0.wfmu.org/freeform-high.aac"
  # @example_site "http://stream0.wfmu.org/freeform-128k"

  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign(
       :form,
       to_form(%{
         # FIXME: default is only for testing
         "station" => @example_site
       })
     )}
  end

  def render(assigns) do
    scanner_form(assigns)
  end

  def scanner_form(assigns) do
    ~H"""
    <div class="flex">
      <div class="w-full sm:w-auto">
        <.form for={@form} phx-submit="preview">
          <div>
            <.label for={@form[:station].id}><%= gettext("Scan station") %></.label>
            <p>
              Enter a station's URL here and it will be scanned for streaming
              services.
            </p>
            <div class="flex flex-row">
              <input
                type="url"
                name="station"
                id={@form[:station].id}
                class={[
                  "w-full text-zinc-900 focus:ring-0 sm:text-sm sm:leading-6",
                  "phx-no-feedback:border-zinc-300 phx-no-feedback:focus:border-zinc-400",
                  "border-zinc-300 focus:border-zinc-400",
                  "rounded-none rounded-l-lg"
                ]}
              />
              <button type="submit" class="p-1 bg-green-300 rounded-r-lg h-full">
                <.icon name="hero-magnifying-glass" />
                <%= gettext("Detect") %>
              </button>
            </div>
          </div>
        </.form>
      </div>
    </div>
    """
  end

  def handle_event("preview", %{"station" => station}, socket) do
    {:noreply,
     socket
     # TODO: Don't URL-encode the parameter
     |> push_navigate(to: ~p"/station?site=#{station}")}
  end
end
