defmodule RadioAtWeb.Player do
  use RadioAtWeb, :live_view
  alias RadioAt.MetadataScraper

  @impl true
  def mount(_, _session, socket) do
    now_playing = RadioAt.Dbus.MediaPlayer2.now_playing()
    station = now_playing["xesam:url"]

    {
      :ok,
      socket
      |> assign(:stream_title, now_playing["rhythmbox:streamTitle"])
      |> assign(:song_title, now_playing["xesam:title"])
      |> assign_async(
        :station,
        fn ->
          {:ok,
           %{
             station:
               station
               |> String.trim()
               |> MetadataScraper.scrape_url()
           }}
        end
      )
    }
  end

  @impl true
  def render(assigns) do
    ~H"""
    <div>Now playing stream: <%= @stream_title %></div>
    <div>Now playing song: <%= @song_title %></div>
    <.async_result :let={station} assign={@station}>
      <:loading>Loading station...</:loading>
      <:failed>Error!</:failed>
      <.station_or_menu station={station} />
    </.async_result>
    """
  end

  @impl true
  def handle_event("remember_station", %{}, socket) do
    {:noreply, socket}
  end
end
